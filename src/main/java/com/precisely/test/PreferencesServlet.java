package com.precisely.test;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.prefs.Preferences;

@WebServlet(name = "PreferencesServlet", urlPatterns = {"/preferences"})
public class PreferencesServlet extends HttpServlet {
    private static final String SYSTEM_KEY = "system-test-key";
    private static final String USER_KEY = "user-test-key";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        ServletOutputStream out = response.getOutputStream();
        Preferences systemRoot = Preferences.systemRoot();
        Preferences userRoot = Preferences.userRoot();

        try {
            systemRoot.put(SYSTEM_KEY, "Some system test value");
        } catch (Exception e) {
            out.println(e.getMessage());
        }

        try {
            userRoot.put(USER_KEY, "Some user test value");
        } catch (Exception e) {
            out.println(e.getMessage());
        }

        String systemPath = System.getProperty("java.util.prefs.systemRoot", "a default value");
        String userPath = System.getProperty("java.util.prefs.userRoot", "a default value");

        String systemValue = systemRoot.get(SYSTEM_KEY, "Could not read value from system preferences.");
        String userValue = userRoot.get(USER_KEY, "Could not read value from user preferences.");

        String javaHome = System.getProperty("java.home", "not set");
        out.print("System Value: ");
        out.println(systemValue);
        out.print("User Value: ");
        out.println(userValue);
        out.print("systemPath is set to ");
        out.println(systemPath);
        out.print("userPath is set to ");
        out.println(userPath);
        out.print("java.home is set to ");
        out.println(javaHome);
    }
}