# Preferences Servlet Project

This project is pretty simple to build. Make sure you have Maven installed and then run:

```mvn package```

If you want to make changes and want fast feedback then the command:

```mvn jetty:run``` will quickly start the servlet.

Using the url http://localhost:8080/preferences will call the servlet.

If you run under Tomcat, using the .war file unchanged, the context will be required so use:

http://localhost:8080/app-1.0-SNAPSHOT.war/preferences/
